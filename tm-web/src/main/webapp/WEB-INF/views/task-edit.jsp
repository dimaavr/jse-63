<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="ru.tsc.avramenko.tm.enumerated.Status" %>
<jsp:include page="../include/_header.jsp"/>
<h1>Task Edit</h1>

<form action="/task/edit/?id=${task.id}" method="post">
<input type="hidden" name="id" value="${task.id}"/>

<p>
<div>Project ID:</div>
<div><select id="projectId" name="projectId">
         <c:forEach var="project" items="${projects}">
         <option value=<c:out value="${project.id}"/>><c:out value="${project.id}"/></option>
         </c:forEach>
         <option hidden value="${task.projectId}" selected="selected">${task.projectId}</option>
     </select>
</div>

<p>
<div>Name:</div>
<div><input type="text" name="name" value="${task.name}"/></div>

<p>
<div>Description:</div>
<div><input type="text" name="description" value="${task.description}"/></div>

<p>
<div>Status:</div>
<div>
<select id="status" name="status">
    <option hidden value="${task.status}" selected="selected">${task.status}</option>
    <option value="Not started">Not started</option>
    <option value="In progress">In progress</option>
    <option value="Complete">Complete</option>
</select></div>

<p>
<div>Start Date:</div>
<div><input type="datetime-local" name="startDate" value=<fmt:formatDate pattern="yyyy-MM-dd'T'HH:mm" value="${task.startDate}"/>></div>

<p>
<div>Finish Date:</div>
<div><input type="datetime-local" name="finishDate" value=<fmt:formatDate pattern="yyyy-MM-dd'T'HH:mm" value="${task.finishDate}"/>></div>

<button type="submit">Save Task</button>
</form>

<jsp:include page="../include/_footer.jsp"/>