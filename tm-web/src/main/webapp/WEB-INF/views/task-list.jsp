<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>
<h1>Task List</h1>

<table width="100%" cellpadding="10" border="1" style="margin-top: 25px">
    <tr>
        <th>ID</th>
        <th>Project ID</th>
        <th>Name</th>
        <th>Desc</th>
        <th>Status</th>
        <th>Start Date</th>
        <th>Finish Date</th>
        <th>Create Date</th>
        <th align="center">Edit</th>
        <th align="center">Delete</th>
    </tr>
    <c:forEach var="task" items="${tasks}">
        <tr>
            <td>
                <c:out value="${task.id}"/>
            </td>
            <td>
                 <c:out value="${task.projectId}"/>
            </td>
            <td>
                <c:out value="${task.name}"/>
            </td>
            <td>
                <c:out value="${task.description}"/>
            </td>
            <td>
                <c:out value="${task.status}"/>
            </td>
            <td>
                <c:out value="${task.startDate}"/>
            </td>
            <td>
                <c:out value="${task.finishDate}"/>
            </td>
            <td>
                <c:out value="${task.created}"/>
            </td>
            <td align="center">
                <a href="/task/edit/?id=${task.id}">Edit</a>
            </td>
            <td align="center">
                <a href="/task/delete/?id=${task.id}">Delete</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/task/create" style="margin-top: 20px">
    <button>Create Task</button>
</form>

<jsp:include page="../include/_footer.jsp"/>