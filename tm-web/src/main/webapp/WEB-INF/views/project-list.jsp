<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>
<h1>Project List</h1>

<table width="100%" cellpadding="9" border="1" style="margin-top: 25px">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Desc</th>
        <th>Status</th>
        <th>Start Date</th>
        <th>Finish Date</th>
        <th>Create Date</th>
        <th align="center">Edit</th>
        <th align="center">Delete</th>
    </tr>
    <c:forEach var="project" items="${projects}">
        <tr>
            <td>
                <c:out value="${project.id}"/>
            </td>
            <td>
                <c:out value="${project.name}"/>
            </td>
            <td>
                <c:out value="${project.description}"/>
            </td>
            <td>
                <c:out value="${project.status}"/>
            </td>
            <td>
                <c:out value="${project.startDate}"/>
            </td>
            <td>
                <c:out value="${project.finishDate}"/>
            </td>
            <td>
                <c:out value="${project.created}"/>
            </td>
            <td align="center">
                <a href="/project/edit/?id=${project.id}">Edit</a>
            </td>
            <td align="center">
                <a href="/project/delete/?id=${project.id}">Delete</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/project/create" style="margin-top: 20px">
    <button>Create Project</button>
</form>

<jsp:include page="../include/_footer.jsp"/>