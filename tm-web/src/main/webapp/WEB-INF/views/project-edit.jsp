<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="ru.tsc.avramenko.tm.enumerated.Status" %>
<jsp:include page="../include/_header.jsp"/>
<h1>Project Edit</h1>

<form action="/project/edit/?id=${project.id}" method="post">
<input type="hidden" name="id" value="${project.id}"/>

<p>
<div>Name:</div>
<div><input type="text" name="name" value="${project.name}"/></div>

<p>
<div>Description:</div>
<div><input type="text" name="description" value="${project.description}"/></div>

<p>
<div>Status:</div>
<div>
<select id="status" name="status">
    <option hidden value="${project.status}" selected="selected">${project.status}</option>
    <option value="Not started">Not started</option>
    <option value="In progress">In progress</option>
    <option value="Complete">Complete</option>
</select></div>

<p>
<div>Start Date:</div>
<div><input type="datetime-local" name="startDate" value=<fmt:formatDate pattern="yyyy-MM-dd'T'HH:mm" value="${project.startDate}"/>></div>

<p>
<div>Finish Date:</div>
<div><input type="datetime-local" name="finishDate" value=<fmt:formatDate pattern="yyyy-MM-dd'T'HH:mm" value="${project.finishDate}"/>></div>

<button type="submit">Save Project</button>
</form>

<jsp:include page="../include/_footer.jsp"/>