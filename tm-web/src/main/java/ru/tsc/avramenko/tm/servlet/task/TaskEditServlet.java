package ru.tsc.avramenko.tm.servlet.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.repository.ProjectRepository;
import ru.tsc.avramenko.tm.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/task/edit/*")
public class TaskEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @Nullable final Task task = TaskRepository.getInstance().findById(id);
        req.setAttribute("projects", ProjectRepository.getInstance().findAll());
        req.setAttribute("task", task);
        req.getRequestDispatcher("/WEB-INF/views/task-edit.jsp").forward(req, resp);
    }

    @Override
    @SneakyThrows
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        @NotNull final String id = req.getParameter("id");
        @NotNull final String name = req.getParameter("name");
        @NotNull final String description = req.getParameter("description");
        @NotNull final String status = req.getParameter("status");
        @NotNull final String startDate = req.getParameter("startDate");
        @NotNull final String finishDate = req.getParameter("finishDate");
        @NotNull final String projectId = req.getParameter("projectId");
        @NotNull final Task task = new Task();
        Date start = date.parse(startDate);
        Date finish = date.parse(finishDate);
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        task.setStatus(status);
        task.setStartDate(start);
        task.setFinishDate(finish);
        task.setProjectId(projectId);
        TaskRepository.getInstance().save(task);
        resp.sendRedirect("/tasks");
    }

}