package ru.tsc.avramenko.tm.repository;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Getter
public class ProjectRepository {

    @NotNull
    private static final ProjectRepository INSTANCE = new ProjectRepository();

    @NotNull
    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    @NotNull
    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        save(new Project("Project1", "Desc1"));
        save(new Project("Project2"));
        save(new Project("Project3", "Desc3"));
    }

    public Project create() {
        return save(new Project());
    }

    public Project save(@NotNull Project project) {
        projects.put(project.getId(), project);
        return project;
    }

    @NotNull
    public Collection<Project> findAll() {
        return projects.values();
    }

    @Nullable
    public Project findById(@NotNull String id) {
        return projects.get(id);
    }

    public void removeById(@Nullable String id) {
        projects.remove(id);
    }

}