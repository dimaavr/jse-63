package ru.tsc.avramenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.avramenko.tm.api.repository.IListenerRepository;
import ru.tsc.avramenko.tm.listener.AbstractListener;

import java.util.*;

@Repository
public class ListenerRepository implements IListenerRepository {

    @NotNull
    private final Map<String, AbstractListener> arguments = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractListener> commands = new LinkedHashMap<>();

    @NotNull
    @Override
    public Collection<AbstractListener> getCommands() {
        return commands.values();
    }

    @NotNull
    @Override
    public Collection<AbstractListener> getArguments() {
        return arguments.values();
    }

    @NotNull
    @Override
    public AbstractListener getCommandByName(String name) {
        return commands.get(name);
    }

    @NotNull
    @Override
    public AbstractListener getCommandByArg(String arg) {
        return arguments.get(arg);
    }

    @Override
    public void add(AbstractListener command) {
        @Nullable final String arg = command.arg();
        @NotNull final String name = command.name();
        if (Optional.ofNullable(arg).isPresent()) arguments.put(arg, command);
        if (Optional.ofNullable(name).isPresent()) commands.put(name, command);
    }

}