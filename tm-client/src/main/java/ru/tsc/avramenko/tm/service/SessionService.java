package ru.tsc.avramenko.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.avramenko.tm.api.service.ISessionService;
import ru.tsc.avramenko.tm.endpoint.Session;
import ru.tsc.avramenko.tm.endpoint.SessionDTO;

@Getter
@Setter
@Service
public class SessionService implements ISessionService {

    @Nullable
    private SessionDTO session;

}